#!/bin/bash

echo "get list of os:Debian from salt"

for h in $(slogin guignol "salt -C 'G@os:Debian' cmd.run 'lsb_release -r'" | xargs -L 2 | cut -d ":" -f1 ); do
  echo ""
  echo $h
  slogin -o connectTimeout=5 -o PasswordAuthentication=no $h ug-upgrade-debian list-old-snapshot
  slogin -o connectTimeout=5 -o PasswordAuthentication=no $h ug-upgrade-debian remove-old-snapshot
done
