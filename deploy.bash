#!/bin/bash

proj_dir="$(dirname $0)"

for h in $(slogin guignol "salt -C 'G@os:Debian' cmd.run 'lsb_release -r'" | xargs -L 2 | cut -d ":" -f1 ); do
  echo $h
  scp -o connectTimeout=5 -o PasswordAuthentication=no ${proj_dir}/ug-upgrade-debian $h:/usr/local/bin/ug-upgrade-debian
  ssh -o connectTimeout=5 -o PasswordAuthentication=no $h chmod +x /usr/local/bin/ug-upgrade-debian
done
